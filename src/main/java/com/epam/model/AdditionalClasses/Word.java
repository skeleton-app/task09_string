package com.epam.model.AdditionalClasses;

public class Word extends DataFormat {

    public static String[] getWords(String sentence) {
        return sentence.split(" ");
    }

    public static char[] getLetters(String word) {
        return word.toCharArray();
    }

    public static boolean checkLetterConsonant(char letter_) {
        char letter = Character.toLowerCase(letter_);
        if (letter == 'b') return true;
        else if (letter == 'c') return true;
        else if (letter == 'd') return true;
        else if (letter == 'f') return true;
        else if (letter == 'g') return true;
        else if (letter == 'h') return true;
        else if (letter == 'j') return true;
        else if (letter == 'k') return true;
        else if (letter == 'l') return true;
        else if (letter == 'm') return true;
        else if (letter == 'n') return true;
        else if (letter == 'p') return true;
        else if (letter == 'q') return true;
        else if (letter == 'r') return true;
        else if (letter == 's') return true;
        else if (letter == 't') return true;
        else if (letter == 'v') return true;
        else if (letter == 'x') return true;
        else if (letter == 'z') return true;
        else if (letter == 'y') return true;
        else if (letter == 'w') return true;
        else return false;
    }
}

