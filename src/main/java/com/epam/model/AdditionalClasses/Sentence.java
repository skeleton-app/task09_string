package com.epam.model.AdditionalClasses;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.epam.Application.logger;

public class Sentence extends DataFormat {
   public List<String> sentenceList;
   private List<String> questionList;
    public Sentence() {
        sentenceList = super.getInputDataList();
    }

    public long countWordsInSentence(String sentence) {
        String[] words = sentence.split(" ");
        return Stream.of(words).count();
    }

    public String findDistinctWordInText(List<String> sentenceList) {
        String[] wordsFromFirstSentence = Word.getWords(sentenceList.get(0));
        String distinctWord = "";
        for (int i = 0; i < wordsFromFirstSentence.length; i++) {
            int counter = 0;
            for (int j = 1; j < sentenceList.size(); j++) {
                if (sentenceList.get(j).contains(wordsFromFirstSentence[i])) break;
                else {
                    counter++;
                    if (counter == sentenceList.size() - 1) {
                        distinctWord = wordsFromFirstSentence[i];
                        return distinctWord;
                    }
                }
            }
        }
        return distinctWord;
    }

    public void printWords(int length) {
        questionList = new LinkedList<>();
        for (int i = 0; i < sentenceList.size(); i++) {
            if (sentenceList.get(i).indexOf('?') != -1) {
                    questionList.add(sentenceList.get(i));
            }
        }
        String[] words;
        char[] letters;
        List<String> finalWords = new ArrayList<>();
        for (int i = 0; i < questionList.size(); i++) {
            words = questionList.get(i).split(" ");
            for (int j = 0; j < words.length; j++) {
                letters = words[j].toCharArray();
                if (length == letters.length) {
                    finalWords.add(words[j]);
                }
            }
        }
        finalWords = finalWords.stream().distinct().collect(Collectors.toList());
        for (int i = 0; i <finalWords.size(); i++) {
            System.out.println(finalWords.get(i));
        }
    }

    public void deleteWords(int length){
        String[] words;
        char[] letters;
        List<String> finalWords = new ArrayList<>();
        for (int i = 0; i < sentenceList.size(); i++) {
            words = sentenceList.get(i).split(" ");
            for (int j = 0; j < words.length; j++) {
                letters = words[j].toCharArray();
                for (int k = 0; k <letters.length ; k++) {
                    letters[k] = Character.toLowerCase(letters[k]);
                }
                if (length == letters.length) {
                    if(!Word.checkLetterConsonant(letters[0])) {
                        finalWords.add(words[j]);
                    }
                }
                else finalWords.add(words[j]);
            }
        }
        for (int i = 0; i <finalWords.size(); i++) {
            System.out.print(finalWords.get(i)+" ");
        }
    }

    public static void main(String[] args) {
        Sentence sentence = new Sentence();
        sentence.deleteWords(6);
    }
}
