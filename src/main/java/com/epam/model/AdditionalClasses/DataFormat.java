package com.epam.model.AdditionalClasses;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DataFormat{
    public File inputDataFile = new File("D:\\EPAM_courses\\task09_string\\src\\main\\resources\\dataFromBook.epam");

    private List<String> inputDataList = new ArrayList<>();

    public DataFormat(){
        try (BufferedReader br = Files.newBufferedReader(Paths.get(inputDataFile.getPath()))) {
            String str;
            inputDataList = br.lines().collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public File getInputDataFile() {
        return inputDataFile;
    }

    public List<String> getInputDataList() {
        return inputDataList;
    }

}
