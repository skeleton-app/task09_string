package com.epam.model;

import com.epam.model.AdditionalClasses.DataFormat;
import com.epam.model.AdditionalClasses.Sentence;

import java.util.List;
import java.util.Scanner;

import static com.epam.Application.logger;

public class BusinessLogic implements Model {

    private Sentence sentence = new Sentence();
    private DataFormat dataFormat = new DataFormat();

    @Override
    public void task1() {

    }

    @Override
    public void task2() {
        List<String> list = dataFormat.getInputDataList();
        String[] sortedByWordsLength = new String[1000];
        long temp = 0;
        for (int i = 0; i < list.size(); i++) {
            for (int j = 0; j < list.size(); j++) {
                if (sentence.countWordsInSentence(list.get(i)) > temp) {
                    temp = sentence.countWordsInSentence(list.get(i));
                    sortedByWordsLength[(int) temp] = list.get(i);
                }
            }
        }
        for (int i = 0; i < sortedByWordsLength.length; i++) {
            if (sortedByWordsLength[i] != null) {
                logger.info(sortedByWordsLength[i]);
            }
        }
    }

    @Override
    public void task3() {
        String task2 = sentence.findDistinctWordInText(sentence.sentenceList);
        logger.info(task2);
    }

    @Override
    public void task4() {
        Scanner in = new Scanner(System.in);
        logger.info("Input the amount of letters: ");
        logger.info("");
        Integer numero = in.nextInt();
            Sentence sentence = new Sentence();
            sentence.printWords(numero);
    }

    @Override
    public void task5() {

    }

    @Override
    public void task6() {

    }

    @Override
    public void task7() {

    }

    @Override
    public void task8() {

    }

    @Override
    public void task9() {

    }

    @Override
    public void task10() {

    }

    @Override
    public void task11() {

    }

    @Override
    public void task12() {
        Scanner in = new Scanner(System.in);
        logger.info("Input the amount of letters: ");
        logger.info("");
        Integer numero = in.nextInt();
        Sentence sentence = new Sentence();
        sentence.deleteWords(numero);
    }

    @Override
    public void task13() {

    }

    @Override
    public void task14() {

    }

    @Override
    public void task15() {

    }

    @Override
    public void task16() {

    }

    @Override
    public void exit() {
        System.exit('0');
    }
}
