package com.epam.view;

import com.epam.controller.Controller;
import com.epam.model.AdditionalClasses.DataFormat;

import java.util.*;
import java.util.stream.Stream;

import static com.epam.Application.logger;

public class MyView {
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private Controller controller;
    private Locale locale;
    private ResourceBundle resourceBundle;

    public MyView(Controller controller) {
        this.controller = controller;
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::task1);
        methodsMenu.put("2", this::task2);
        methodsMenu.put("3", this::task3);
        methodsMenu.put("4", this::task4);
        methodsMenu.put("5", this::task5);
        methodsMenu.put("6", this::task6);
        methodsMenu.put("7", this::task7);
        methodsMenu.put("8", this::task8);
        methodsMenu.put("9", this::task9);
        methodsMenu.put("10", this::task10);
        methodsMenu.put("11", this::task11);
        methodsMenu.put("12", this::task12);
        methodsMenu.put("13", this::task13);
        methodsMenu.put("14", this::task14);
        methodsMenu.put("15", this::task15);
        methodsMenu.put("16", this::task16);
        methodsMenu.put("17", this::settings);
        methodsMenu.put("Q", this::exit);
        locale = new Locale("en");
        resourceBundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        show();
    }


    private void task1() {
        controller.task1();
    }

    private void task2() {
        controller.task2();
    }

    private void task3() {
        controller.task3();
    }

    private void task4() {
        controller.task4();
    }

    private void task5() {
        controller.task5();
    }

    private void task6() {
        controller.task6();
    }

    private void task7() {
        controller.task7();
    }

    private void task8() {
        controller.task8();
    }

    private void task9() {
        controller.task9();
    }

    private void task10() {
        controller.task10();
    }

    private void task11() {
        controller.task11();
    }

    private void task12() {
        controller.task12();
    }

    private void task13() {
        controller.task13();
    }

    private void task14() {
        controller.task14();
    }

    private void task15() {
        controller.task15();
    }

    private void task16() {
        controller.task16();
    }

    private void localeMenuEN() {
        logger.info("Please choose your language: ");
        logger.info("1 - Ukrainian");
        logger.info("2 - Russian");
        logger.info("3 - Japanese");
        logger.info("4 - Polish");
        logger.info("5 - English");
    }

    private void localeMenuJP() {
        logger.info("言語を選択してください：");
        logger.info("1-英語");
        logger.info("2-ロシア語");
        logger.info("3-日本");
        logger.info("4-ポーランド");
        logger.info("5 - 英語");


    }

    private void localeMenuPL() {
        logger.info("Proszę wybrać język:");
        logger.info("1 - Angielski");
        logger.info("2 - Rosyjski");
        logger.info("3 - Japonia");
        logger.info("4 - Polska");
        logger.info("5 - English");

    }

    private void localeMenuUK() {
        logger.info("Будь ласка, виберіть вашу мову:");
        logger.info("1 - Українська");
        logger.info("2 - Російська");
        logger.info("3 - Японська");
        logger.info("4 - Польська");
        logger.info("5 - English");

    }

    private void localeMenuRU() {
        logger.info("Пожалуйста, выберите ваш язык:");
        logger.info("1 - Украинский");
        logger.info("2 - Русский");
        logger.info("3 - Японский");
        logger.info("4 - Польcкий");
        logger.info("5 - English");

    }

    private Locale localise(String language, Locale locale) {
        if (language.equals("1")) {
            return new Locale("uk");
        }
        if (language.equals("2")) {
            return new Locale("ru");
        }
        if (language.equals("3")) {
            return new Locale("jp");
        }
        if (language.equals("4")) {
            return new Locale("pl");
        } else {
            return new Locale("en");
        }
    }

    private Locale changeLocale() {
        if (locale != null) {
            String currentLocale = String.valueOf(locale);
            switch (currentLocale) {
                case "uk": {
                    localeMenuUK();
                    String language = input.nextLine();
                    locale = localise(language, locale);
                    return locale;
                }
                case "ru": {
                    localeMenuRU();
                    String language = input.nextLine();
                    locale = localise(language, locale);
                    return locale;
                }
                case "jp": {
                    localeMenuJP();
                    String language = input.nextLine();
                    locale = localise(language, locale);
                    return locale;
                }
                case "pl": {
                    localeMenuPL();
                    String language = input.nextLine();
                    locale = localise(language, locale);
                    return locale;
                }
                case "en":
                default: {
                    localeMenuEN();
                    String language = input.nextLine();
                    locale = localise(language, locale);
                    return locale;
                }
            }
        }
        return new Locale("en");
    }

    private void settings() {
        locale = changeLocale();
        resourceBundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        show();
    }

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", resourceBundle.getString("1"));
        menu.put("2", resourceBundle.getString("2"));
        menu.put("3", resourceBundle.getString("3"));
        menu.put("4", resourceBundle.getString("4"));
        menu.put("5", resourceBundle.getString("5"));
        menu.put("6", resourceBundle.getString("6"));
        menu.put("7", resourceBundle.getString("7"));
        menu.put("8", resourceBundle.getString("8"));
        menu.put("9", resourceBundle.getString("9"));
        menu.put("10", resourceBundle.getString("10"));
        menu.put("11", resourceBundle.getString("11"));
        menu.put("12", resourceBundle.getString("12"));
        menu.put("13", resourceBundle.getString("13"));
        menu.put("14", resourceBundle.getString("14"));
        menu.put("15", resourceBundle.getString("15"));
        menu.put("16", resourceBundle.getString("16"));
        menu.put("17", resourceBundle.getString("17"));
        menu.put("Q", resourceBundle.getString("Q"));
    }

    private void exit() {
        controller.exit();
    }

    private void outputMenu() {
        logger.info("\nMENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            this.outputMenu();
            logger.info("Press, number or key < Q >");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                logger.error("ERROR. " + e);
            }
        }
        while (!keyMenu.equals("Q"));
    }
}
