package com.epam.controller;

import com.epam.model.Model;

import static com.epam.Application.logger;


public class ControllerImpl implements Controller {
    private Model model;

    public ControllerImpl(Model model) {
        this.model = model;
    }

    @Override
    public void task1() {
        model.task1();
    }

    @Override
    public void task2() {
        model.task2();
    }

    @Override
    public void task3() {
        model.task3();
    }

    @Override
    public void task4() {
        model.task4();
    }

    @Override
    public void task5() {
        model.task5();
    }

    @Override
    public void task6() {
        model.task6();
    }

    @Override
    public void task7() {
        model.task7();
    }

    @Override
    public void task8() {
        model.task8();
    }

    @Override
    public void task9() {
        model.task9();
    }

    @Override
    public void task10() {
        model.task10();
    }

    @Override
    public void task11() {
        model.task11();
    }

    @Override
    public void task12() {
        model.task12();
    }

    @Override
    public void task13() {
        model.task13();
    }

    @Override
    public void task14() {
        model.task14();
    }

    @Override
    public void task15() {
        model.task15();
    }

    @Override
    public void task16() {
        model.task16();
    }

    @Override
    public void exit() {
        model.exit();
    }
}
